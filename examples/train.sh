#!/usr/bin/env bash

# Assigning command line arguments to variables
BASE_DIR=$1
BASE_MODEL=$2
WANDB_API_KEY=$3
WANDB_PROJECT=$4

# Exporting the environment variables
export BASE_DIR
export BASE_MODEL
export WANDB_API_KEY
export WANDB_PROJECT

# Login to Weights & Biases
wandb login $WANDB_API_KEY

# Running the training script with the provided parameters
torchrun --nnodes=1 $BASE_DIR/qlora/train.py \
  --model_name_or_path $BASE_MODEL \
  --working_dir $BASE_DIR/$WANDB_PROJECT-checkpoints \
  --output_dir $BASE_DIR/$WANDB_PROJECT-lora \
  --num_train_epochs 5 \
  --logging_steps 1 \
  --save_strategy steps \
  --save_steps 15 \
  --save_total_limit 1 \
  --data_seed 11422 \
  --evaluation_strategy steps \
  --eval_dataset_size 0.02 \
  --eval_steps 5 \
  --max_new_tokens 4096 \
  --dataloader_num_workers 1 \
  --logging_strategy steps \
  --optim adamw_torch \
  --do_train \
  --bits 4 \
  --bf16 \
  --dataset $BASE_DIR/training_set.jsonl \
  --dataset_format input-output \
  --model_max_len 4096 \
  --per_device_train_batch_size 1 \
  --learning_rate 2e-5 \
  --lr_scheduler_type cosine \
  --warmup_ratio 0.005 \
  --weight_decay 0.0 \
  --seed 11422 \
  --deepspeed $BASE_DIR/deepspeed-7b.json \
  --report_to wandb \
  --gradient_checkpointing \
  --use_flash_attention_2

tar -I zstd -cvf $WANDB_PROJECT.tar.zstd $BASE_DIR/$WANDB_PROJECT-lora/

python qlora/merge.py --base $BASE_MODEL --peft $BASE_DIR/$WANDB_PROJECT-lora/checkpoint-*/ --out $BASE_DIR/$WANDB_PROJECT

tar -I zstd -cvf $WANDB_PROJECT.tar.zstd $BASE_DIR/$WANDB_PROJECT/
