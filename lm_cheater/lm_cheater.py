import gc
import subprocess
import random
import os
import json

# Importing necessary libraries
import evaluate
import pandas as pd
from transformers import AutoTokenizer, pipeline, enable_full_determinism
from transformers.pipelines.pt_utils import KeyDataset
from datasets import load_dataset
from tqdm.notebook import tqdm
import matplotlib.pyplot as plt
import numpy as np

# Setting a fixed seed for reproducibility
SEED = 1337
enable_full_determinism(SEED)
random.seed(SEED)

# Function to save data to a file
def save_data(data, filename):
    with open(filename, 'w') as f:
        json.dump(data, f)

# Function to load data from a file
def load_data(filename):
    with open(filename, 'r') as f:
        return json.load(f)

# Function to mark the start time of a process
def time_start():
    return datetime.now()

# Function to calculate and display elapsed time since a start time
def time_elapsed_since(start):
    end_time = datetime.now()  # End time
    duration = end_time - start_time
    tqdm.write(f"Start: {start_time.strftime('%Y-%m-%d %H:%M:%S')} End: {end_time.strftime('%Y-%m-%d %H:%M:%S')} Duration: {duration}")

# Main function to compare different models
def compare(categories, category_prompts, repos, sample_count):
    avg_max = 20

    # Inner function to generate continuations using a pipeline
    def gen_continuations(pipe, prompts):
        ds = KeyDataset(prompts, "prompt")
        continuations = []
        for generation in tqdm(pipe(ds, max_length=50, return_full_text=False, do_sample=True), desc="Generating continuations"):
            continuation = generation[0]['generated_text']
            continuations.append(continuation)
        return continuations

    diffs = []
    for repo in tqdm(repos, desc="Processing repos", dynamic_ncols=True):
        tqdm.write(f"Current repository: {repo}")

        # Checkpoint handling
        checkpoint = f"continuations-ds-{repo.split('/')[1]}-{sample_count}.json"
        continuations = []
        if os.path.isfile(checkpoint):
            tqdm.write(f"Using checkpoint {checkpoint}")
            continuations = load_data(checkpoint)
        else:
            # Initializing pipeline and tokenizer
            pipe = pipeline(model=repo, device_map="cuda:0")
            tokenizer = AutoTokenizer.from_pretrained(repo)

            # Generating continuations for each category
            for category, prompts in tqdm(zip(categories, category_prompts), total=len(categories)):
                tqdm.write(f"Current category: {category}")
                continuations.append(gen_continuations(pipe, prompts))

            save_data(continuations, checkpoint)

        # Regard benchmark loading and computation
        tqdm.write("Loading regard benchmark...")
        regard = evaluate.load("./evaluate/measurements/regard", 'compare')

        tqdm.write("Computing regard differences...")
        da = {'positive': 0, 'neutral': 0, 'negative': 0, 'other': 0}
        for i in tqdm(range(avg_max)):
            diff = regard.compute(data=continuations[1], references=continuations[0])
            for k, v in diff["regard_difference"].items():
                da[k] += v

        for k, v in da.items():
            da[k] /= avg_max
        diff = {repo: da}
        print(diff)

        diffs.append(diff)

        # Clearing memory
        pipe = None
        tokenizer = None
        regard = None
        gc.collect()

    return diffs

# Function to convert raw data into a pandas DataFrame
def convert_to_dataframe(data):
    # Preparing data for DataFrame
    transformed_data = []
    for item in data:
        for repo, scores in item.items():
            transformed_data.append({
                'Repo': repo,
                'Positive': scores.get('positive', 0),
                'Neutral': scores.get('neutral', 0),
                'Negative': scores.get('negative', 0),
                'Other': scores.get('other', 0)
            })

    # Creating DataFrame
    df = pd.DataFrame(transformed_data)
    return df

# Function to create a test set
def create_test_set(top_dataset, categories, sample_count):
    data = load_dataset(top_dataset, split="train")

    category_lens = {}
    category_prompts = []
    for category in categories:
        # Sampling and filtering data by category
        sampled = [p for p in data if p['category'] == category]
        category_len = len(sampled)
        category_lens[category] = category_len

        # Determining sample size
        if sample_count != 0:
            sampled = random.sample(sampled, min(sample_count, category_len))
        else:
            max_len = max(category_lens.values())
            sampled = random.sample(sampled, min(max_len, category_len))

        # Creating prompts for each category
        category_prompts.append([{"prompt": p['prompts'][0]} for p in sampled])
    return category_prompts

# Function to create a training set
def create_train_set(top_dataset, categories):
    data = load_dataset(top_dataset, split="train")
    data.select_columns(['prompts', 'wikipedia', 'category'])

    training_set = []
    for category in categories:
        # Constructing training set based on category
        for p, w, c in zip(data["prompts"], data["wikipedia"], data["category"]):
            if category == c:
                training_set.append({"input": p[0], "output": w[0].split(p[0])[1]})

    return training_set

# Function to evaluate models using MMLU
def mmlu(repos):
    for repo in repos:
        cmd = [
            "lm_eval", "--model", "hf", "--model_args",
            f"pretrained={repo}", "--tasks", "mmlu", "--device",
            "cuda:0", "--batch_size", "auto:4"
        ]

        with open(f"{repo.split('/')[1]}_output.txt", "w") as outfile:
            process = subprocess.Popen(
                cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, text=True
            )

            # Real-time logging of the output
            for line in iter(process.stdout.readline, ''):
                print(line, end='')
                outfile.write(line)

            process.stdout.close()
            process.wait()

# Alternative implementation of mmlu using lm_eval library directly
def mmlu(repo):
    import argparse
    from lm_eval.__main__ import cli_evaluate, parse_eval_args

    # Preparing default arguments
    default_args = parse_eval_args()
    default_args.model = "hf"
    default_args.model_args = f"pretrained={repo}"
    default_args.tasks = "mmlu"
    default_args.device = "cuda:0"
    default_args.batch_size = "auto:4"
    default_args.verbosity = "INFO"

    # Executing the evaluation
    cli_evaluate(default_args)

# Function to run a specific command
def run_command(repo):
    command = [
        'lm_eval',
        '--model', 'hf',
        '--model_args', f'pretrained={repo}',
        '--tasks', 'mmlu',
        '--device', 'cuda:0',
        '--batch_size', 'auto:4'
    ]

    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)

    # Real-time output handling
    while True:
        output = process.stdout.readline()
        if output == '' and process.poll() is not None:
            break
        if output:
            print(output.strip())

    # Error handling
    stderr = process.stderr.read()
    if stderr:
        print(stderr)

# Function to visualize results in a bar chart
def draw_results(diffs, categories, outfile):
    # Preparing data for plotting
    sentiments = ['positive', 'neutral', 'negative']
    repos = [list(d.keys())[0] for d in diffs]
    values = {sentiment: [d.get(repo, {}).get(sentiment, 0) for repo, d in zip(repos, diffs)] for sentiment in sentiments}

    # Setting up bar chart parameters
    n_groups = len(sentiments)
    bar_width = 0.2
    opacity = 0.8
    index = np.arange(n_groups)

    # Rearranging values for plotting
    values = {repo: {sentiment: diffs[i][repo].get(sentiment, 0) for sentiment in sentiments} for i, repo in enumerate(repos)}

    # Plotting bars
    fig, ax = plt.subplots()
    for i, repo in enumerate(repos):
        repo_values = [values[repo][sentiment] for sentiment in sentiments]
        ax.bar(index + i * bar_width, repo_values, bar_width, alpha=opacity, label=repo)

    # Setting labels and title
    ax.set_xlabel('Category')
    ax.set_ylabel('Scores')
    ax.set_title(f'{categories[1]} sentiment / {categories[0]} sentiment')
    ax.set_xticks(index + bar_width / 2 * (len(repos) - 1))
    ax.set_xticklabels(sentiments)
    ax.legend()

    # Displaying the plot
    fig.savefig(outfile, format='svg')
    plt.show()
