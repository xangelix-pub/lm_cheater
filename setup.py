from setuptools import setup, find_packages

setup(
    name='lm_cheater',
    version='0.1',
    packages=find_packages(),
    description='Cheap LLM overfitting.',
    url='https://gitlab.com/xangelix-pub/lm_cheater',
    author='Cody Wyatt Neiman',
    author_email='neiman@cody.to',
    license='GPLv3',
    install_requires=[
        "transformers==4.36.2",
        "bitsandbytes==0.41.3.post2",
        "accelerate==0.25.0",
        "einops==0.7.0",
        "peft==0.7.1",
        "datasets==2.15.0",
        "evaluate==0.4.1",
        "scikit-learn==1.3.2",
        "sentencepiece==0.1.99",
        "matplotlib==3.8.2",
        "deepspeed==0.12.5",
        "flash_attn==2.3.6",
        "protobuf==3.20.3",
        "wandb==0.16.1",
        "huggingface-hub==0.19.4",
        "tqdm==4.66.1",
        "numpy==1.26.2",
        "torch==2.1.2",
        "pandas==2.1.4"
    ],
    zip_safe=False
)
